<?php require("db.php"); ?>

<!DOCTYPE html>
<html lang="de">
  	<head>
  		<base href="//projects.jdittmer.com/MaxH/order_sys/"></base>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Order System</title>

		<!-- Bootstrap -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
		<link href="assets/css/pricing-table-global.css" rel="stylesheet">
		<link href="assets/css/demo.css" rel="stylesheet">

		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
  	</head>
  	<body>
		<br>
		<?php
			if(!$_GET['s']){require("assets/inc/main/main.php");}
			if($_GET['s'] == "Gameserver"){require("assets/inc/main/gameserver.php");}
			if($_GET['s'] == "Voiceserver"){require("assets/inc/main/voiceserver.php");}

			if($_GET['s'] == "CheckOut" && $_GET['site'] == "Gameserver"){require("assets/inc/checkout/gameserver.php");}
		?>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
  	</body>
</html>
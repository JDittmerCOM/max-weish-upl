<div class="container-fluid">
	<div class="row">
		<div class="col-md-9 span-centered">
			<div class="row">
				<div class="col-md-4 pricing-table belize-hole bordered">
					<?php 
						$sql_one = mysqli_query($db, "SELECT * FROM jdro_voiceserver_tbl WHERE id = 1");
						$row_one = mysqli_fetch_assoc($sql_one);
						if($row_one['ddos'] == 0){
							$one_ddos = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$one_ddos = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_one['auto_backups'] == 0){
							$one_auto_backups = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$one_auto_backups = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_one['fixed_serveraddress_port'] == 0){
							$one_fixed_serveraddress_port = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$one_fixed_serveraddress_port = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}
					?>
					<ul>
						<li class="title"><?php echo $row_one['product_name']; ?></li>
						<li class="price">
							<span class="currency-symbol">€</span>
							<strong>0</strong>
							<sup>00</sup>
							<em>/Mtl.</em>
						</li>
						<li><span class="pull-right"><?php echo $row_one['slots']; ?></span>Slotanzahl</li>
						<li><span class="pull-right"><?php echo $one_ddos; ?></span>DDoS Protection</li>
						<li><span class="pull-right"><?php echo $row_one['run_time']; ?></span>Laufzeit</li>
						<li><span class="pull-right"><?php echo $row_one['soundquality']; ?></span>Soundqualität (Codecs)</li>
						<li><span class="pull-right"><?php echo $row_one['cp']; ?></span>Control-Panel</li>
						<li><span class="pull-right"><?php echo $one_auto_backups; ?></span>Autom. Backups</li>
						<li><span class="pull-right"><?php echo $one_fixed_serveraddress_port; ?></span>Feste Serveradresse mit Port</li>
						<li class="button">
							<button class="btn btn-large">
								<i class="fa fa-shopping-cart fa-fw"></i>&nbsp;&nbsp;Bestellen
							</button>
						</li>
					</ul>
				</div>

				<div class="col-md-4 pricing-table belize-hole bordered">
					<?php 
						$sql_two = mysqli_query($db, "SELECT * FROM jdro_voiceserver_tbl WHERE id = 2");
						$row_two = mysqli_fetch_assoc($sql_two);
						if($row_two['ddos'] == 0){
							$two_ddos = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$two_ddos = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_two['auto_backups'] == 0){
							$two_auto_backups = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$two_auto_backups = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_two['fixed_serveraddress_port'] == 0){
							$two_fixed_serveraddress_port = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$two_fixed_serveraddress_port = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}
					?>
					<ul>
						<li class="title"><?php echo $row_two['product_name']; ?></li>
						<li class="price">
							<span class="currency-symbol">€</span>
							<strong>0</strong>
							<sup>00</sup>
							<em>/Mtl.</em>
						</li>
						<li><span class="pull-right"><?php echo $row_two['slots']; ?></span>Slotanzahl</li>
						<li><span class="pull-right"><?php echo $two_ddos; ?></span>DDoS Protection</li>
						<li><span class="pull-right"><?php echo $row_two['run_time']; ?></span>Laufzeit</li>
						<li><span class="pull-right"><?php echo $row_two['soundquality']; ?></span>Soundqualität (Codecs)</li>
						<li><span class="pull-right"><?php echo $row_two['cp']; ?></span>Control-Panel</li>
						<li><span class="pull-right"><?php echo $two_auto_backups; ?></span>Autom. Backups</li>
						<li><span class="pull-right"><?php echo $two_fixed_serveraddress_port; ?></span>Feste Serveradresse mit Port</li>
						<li class="button">
							<button class="btn btn-large">
								<i class="fa fa-shopping-cart fa-fw"></i>&nbsp;&nbsp;Bestellen
							</button>
						</li>
					</ul>
				</div>

				<div class="col-md-4 pricing-table belize-hole bordered">
					<?php 
						$sql_three = mysqli_query($db, "SELECT * FROM jdro_voiceserver_tbl WHERE id = 3");
						$row_three = mysqli_fetch_assoc($sql_three);
						if($row_three['ddos'] == 0){
							$three_ddos = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$three_ddos = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_three['auto_backups'] == 0){
							$three_auto_backups = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$three_auto_backups = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_three['fixed_serveraddress_port'] == 0){
							$three_fixed_serveraddress_port = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$three_fixed_serveraddress_port = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}
					?>
					<ul>
						<li class="title"><?php echo $row_three['product_name']; ?></li>
						<li class="price">
							<span class="currency-symbol">€</span>
							<strong>0</strong>
							<sup>00</sup>
							<em>/Mtl.</em>
						</li>
						<li><span class="pull-right"><?php echo $row_three['slots']; ?></span>Slotanzahl</li>
						<li><span class="pull-right"><?php echo $three_ddos; ?></span>DDoS Protection</li>
						<li><span class="pull-right"><?php echo $row_three['run_time']; ?></span>Laufzeit</li>
						<li><span class="pull-right"><?php echo $row_three['soundquality']; ?></span>Soundqualität (Codecs)</li>
						<li><span class="pull-right"><?php echo $row_three['cp']; ?></span>Control-Panel</li>
						<li><span class="pull-right"><?php echo $three_auto_backups; ?></span>Autom. Backups</li>
						<li><span class="pull-right"><?php echo $three_fixed_serveraddress_port; ?></span>Feste Serveradresse mit Port</li>
						<li class="button">
							<button class="btn btn-large">
								<i class="fa fa-shopping-cart fa-fw"></i>&nbsp;&nbsp;Bestellen
							</button>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
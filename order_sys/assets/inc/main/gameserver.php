<div class="container-fluid">
	<div class="row">
		<div class="col-md-9 span-centered">
			<div class="row">
				<div class="col-md-4 pricing-table belize-hole bordered">
					<?php 
						$sql_one = mysqli_query($db, "SELECT * FROM jdro_gameserver_tbl WHERE id = 1");
						$row_one = mysqli_fetch_assoc($sql_one);
						if($row_one['auto_backups'] == 0){
							$one_auto_backups = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$one_auto_backups = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_one['own_ip_port'] == 0){
							$one_own_ip_port = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$one_own_ip_port = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_one['own_servername'] == 0){
							$one_own_servername = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$one_own_servername = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}
					?>
					<ul>
						<li class="title"><?php echo $row_one['product_name']; ?></li>
						<li class="price">
							<span class="currency-symbol">€</span>
							<strong>0</strong>
							<sup>00</sup>
							<em>/Mtl.</em>
						</li>
						<li><span class="pull-right"><?php echo $row_one['slots']; ?></span>Slotanzahl</li>
						<li><span class="pull-right"><?php echo $row_one['ram']; ?></span>RAM</li>
						<li><span class="pull-right"><?php echo $row_one['traffic']; ?></span>Traffic</li>
						<li><span class="pull-right"><?php echo $row_one['band_width']; ?></span>Bandbreite</li>
						<li><span class="pull-right"><?php echo $row_one['cp']; ?></span>Control-Panel</li>
						<li><span class="pull-right"><?php echo $one_auto_backups; ?></span>Autom. Backups</li>
						<li><span class="pull-right"><?php echo $one_own_ip_port; ?></span>Eigene IP mit Port</li>
						<li><span class="pull-right"><?php echo $one_own_servername; ?></span>Eigener Servername</li>
						<li class="button">
							<form action="CheckOut/Gameserver/1" method="post">
								<input type="hidden" name="cat" value="Gameserver">
								<input type="hidden" name="podtID" value="1">
								<button type="submit" class="btn btn-large"><i class="fa fa-shopping-cart fa-fw"></i>&nbsp;&nbsp;Bestellen</button>
							</form>
						</li>
					</ul>
				</div>

				<div class="col-md-4 pricing-table belize-hole bordered">
					<?php 
						$sql_two = mysqli_query($db, "SELECT * FROM jdro_gameserver_tbl WHERE id = 2");
						$row_two = mysqli_fetch_assoc($sql_two);
						if($row_two['auto_backups'] == 0){
							$two_auto_backups = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$two_auto_backups = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_two['own_ip_port'] == 0){
							$two_own_ip_port = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$two_own_ip_port = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_two['own_servername'] == 0){
							$two_own_servername = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$two_own_servername = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}
					?>
					<ul>
						<li class="title"><?php echo $row_two['product_name']; ?></li>
						<li class="price">
							<span class="currency-symbol">€</span>
							<strong>0</strong>
							<sup>00</sup>
							<em>/Mtl.</em>
						</li>
						<li><span class="pull-right"><?php echo $row_two['slots']; ?></span>Slotanzahl</li>
						<li><span class="pull-right"><?php echo $row_two['ram']; ?></span>RAM</li>
						<li><span class="pull-right"><?php echo $row_two['traffic']; ?></span>Traffic</li>
						<li><span class="pull-right"><?php echo $row_two['band_width']; ?></span>Bandbreite</li>
						<li><span class="pull-right"><?php echo $row_two['cp']; ?></span>Control-Panel</li>
						<li><span class="pull-right"><?php echo $two_auto_backups; ?></span>Autom. Backups</li>
						<li><span class="pull-right"><?php echo $two_own_ip_port; ?></span>Eigene IP mit Port</li>
						<li><span class="pull-right"><?php echo $two_own_servername; ?></span>Eigener Servername</li>
						<li class="button">
							<button class="btn btn-large">
								<i class="fa fa-shopping-cart fa-fw"></i>&nbsp;&nbsp;Bestellen
							</button>
						</li>
					</ul>
				</div>

				<div class="col-md-4 pricing-table belize-hole bordered">
					<?php 
						$sql_three = mysqli_query($db, "SELECT * FROM jdro_gameserver_tbl WHERE id = 3");
						$row_three = mysqli_fetch_assoc($sql_three);
						if($row_three['auto_backups'] == 0){
							$three_auto_backups = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$three_auto_backups = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_three['own_ip_port'] == 0){
							$three_own_ip_port = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$three_own_ip_port = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}

						if($row_three['own_servername'] == 0){
							$three_own_servername = "<span class='text-danger'><i class='fa fa-times fa-fw'></i></span>";
						}else{
							$three_own_servername = "<span class='text-success'><i class='fa fa-check fa-fw'></i></span>";
						}
					?>
					<ul>
						<li class="title"><?php echo $row_three['product_name']; ?></li>
						<li class="price">
							<span class="currency-symbol">€</span>
							<strong>0</strong>
							<sup>00</sup>
							<em>/Mtl.</em>
						</li>
						<li><span class="pull-right"><?php echo $row_three['slots']; ?></span>Slotanzahl</li>
						<li><span class="pull-right"><?php echo $row_three['ram']; ?></span>RAM</li>
						<li><span class="pull-right"><?php echo $row_three['traffic']; ?></span>Traffic</li>
						<li><span class="pull-right"><?php echo $row_three['band_width']; ?></span>Bandbreite</li>
						<li><span class="pull-right"><?php echo $row_three['cp']; ?></span>Control-Panel</li>
						<li><span class="pull-right"><?php echo $three_auto_backups; ?></span>Autom. Backups</li>
						<li><span class="pull-right"><?php echo $three_own_ip_port; ?></span>Eigene IP mit Port</li>
						<li><span class="pull-right"><?php echo $three_own_servername; ?></span>Eigener Servername</li>
						<li class="button">
							<button class="btn btn-large">
								<i class="fa fa-shopping-cart fa-fw"></i>&nbsp;&nbsp;Bestellen
							</button>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
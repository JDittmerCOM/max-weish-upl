<div class="container">
	<?php
		setcookie("podtID", $_POST['podtID'], time()+3600, "/", ".".$_SERVER['SERVER_NAME']);

		if($_POST['podtID'] == 1 && ONLINE != 1){
			if(isset($_POST['register_sub'])){
				$kdn_id = rand(1000000,9999999);
				$date = time();
				$ip = $_SERVER['REMOTE_ADDR'];
				$firstname = mysqli_real_escape_string($db, $_POST['firstname']);
				$lastname = mysqli_real_escape_string($db, $_POST['lastname']);
				$email1 = mysqli_real_escape_string($db, $_POST['email1']);
				$email2 = mysqli_real_escape_string($db, $_POST['email2']);
				$pass1 = mysqli_real_escape_string($db, $_POST['pass1']);
				$pass2 = mysqli_real_escape_string($db, $_POST['pass2']);
				$street = mysqli_real_escape_string($db, $_POST['street']);
				$house_number = mysqli_real_escape_string($db, $_POST['house_number']);
				$zip = mysqli_real_escape_string($db, $_POST['zip']);
				$place = mysqli_real_escape_string($db, $_POST['place']);
				$birth_d = mysqli_real_escape_string($db, $_POST['birth_d']);
				$birth_m = mysqli_real_escape_string($db, $_POST['birth_m']);
				$birth_y = mysqli_real_escape_string($db, $_POST['birth_y']);
					$birthday = $birth_d.". ".$birth_m." ".$birth_y;
				$country = mysqli_real_escape_string($db, $_POST['country']);
				$phone_number = mysqli_real_escape_string($db, $_POST['phone_number']);
				$mobile_number = mysqli_real_escape_string($db, $_POST['mobile_number']);

				$users_sql = mysqli_query($db, "SELECT * FROM jdro_users WHERE email = '".$email1."'");
				$users_row = mysqli_fetch_assoc($users_sql);

				//CHECK FIRSTNAME
				if(!preg_match("/(^[a-zA-Z ]$)/", $firstname)){
					echo bad("Ihr Vorname darf nur <strong>a-z A-Z (Leerzeichen)</strong> enthalten!");
				}else{
					$check__firstname = true;
				}

				if(strlen($firstname) < 3){
					echo bad("Ihr Vorname muss mindestens 3 Zeichen enthalten!");
				}else{
					$check__firstname2 = true;
				}

				//CHECK LASTNAME
				if(!preg_match("/(^[a-zA-Z ]$)/", $lastname)){
					echo bad("Ihr Nachname darf nur <strong>a-z A-Z (Leerzeichen)</strong> enthalten!");
				}else{
					$check__lastname = true;
				}

				if(strlen($lastname) < 3){
					echo bad("Ihr Nachname muss mindestens 3 Zeichen enthalten!");
				}else{
					$check__lastname2 = true;
				}

				//CHECK EMAIL
                if(strlen($email1) < 8){
                    echo bad("Ihre E-Mail Adresse muss mindestens 8 Zeichen enthalten!");
                }else{
                	$check__email = true;
                }

                if($email1 == $users_row['email']){
                    echo bad("Ihre eMail Adresse ist bereits vergeben!");
                }else{
                	$check__email2 = true;
                }

                if(!preg_match("/^([a-zA-Z0-9\-\.]+)\@([a-zA-Z0-9\-]+)\.([a-zA-Z0-9\.]{2,12})$/", $email1)){
                    echo bad("Ihre eMail Adresse enthält ungültige Zeichen!");
                }else{
                	$check__email3 = true;
                }

                //CHECK PASSWORD
                if(strlen($pass1) < 8){
                    echo bad("Ihr Passwort muss mindestens 8 Zeichen enthalten!");
                }else{
                	$check__password = true;
                }

                if($pass1 != $pass2){
                    echo bad("Ihre Passwörter stimmen nicht überein.");
                }else{
                	$check__password2 = true;
                }

                if($check__password == true && $check__password2 == true){
                    $pass1 = hash("sha512", $pass1);
                }

				//CHECK STREET
				if(!preg_match("/(^[a-zA-Z0-9\-\ß ]+$)/", $street)){
					echo bad("Der Straßenname darf nur <strong>a-z A-Z 0-9 - ß (Leerzeichen)</strong> enthalten!");
				}else{
					$check__street = true;
				}

				if(strlen($street) < 3){
					echo bad("Der Straßenname muss mindestens 3 Zeichen enthalten!");
				}else{
					$check__street2 = true;
				}

				//CHECK ZIP
                if(strlen($zip) < 3){
                    echo bad("Ihre Postleitzahl muss mindestens 3 Zeichen enthalten!");
                }else{
                	$check__zip = true;
                }

                if(!preg_match("/^([0-9]{3,5})$/", $zip)){
                    echo bad("Ihre Postleitzahl enthält ungültige Zeichen. Bitte verwenden Sie nur <strong>0-9</strong>.");
                }else{
                	$check__zip2 = true;
                }

                //CHECK PLACE
                if(strlen($place) < 2){
                    echo bad("Ihr Wohnort muss mindestens 2 Zeichen enthalten!");
                }else{
                	$check__place = true;
                }

                if(!preg_match("/([a-zA-Z\- ])+/", $place)){
                    echo bad("Ihr Wohnort enthält ungültige Zeichen. Bitte verwenden Sie nur <strong>a-z A-Z -</strong>.");
                }else{
                	$check__place2 = true;
                }


                if($check__firstname == true && $check__firstname2 == true
                	&& $check__lastname == true && $check__lastname2 == true
                	&& $check__email == true && $check__email2 == true && $check__email3 == true
                	&& $check__password == true && $check__password2 == true
                	&& $check__street == true && $check__street2 == true
                	&& $check__zip == true && $check__zip2 == true
                	&& $check__place == true && $check__place2 == true){
                	mysqli_query($db, "INSERT INTO jdro_users (kdn_id, 
                											   `date`, 
                											   ip, 
                											   firstname, 
                											   lastname,
                											   email,
                											   password,
                											   street,
                											   zip,
                											   birthday,
                											   place) 
                							VALUES ('".$kdn_id."', 
                									'".$date."',
                									'".$ip."',
                									'".$firstname."',
                									'".$lastname."',
                									'".$email1."',
                									'".$pass1."',
                									'".$street."',
                									'".$zip."',
                									'".$birthday."',
                									'".$place."')
                	");
                	if(mysql_error()){exit(mysql_error());}
                }
			}

			echo "<form method='post'>";
				echo "<div class='form-group'>";
					echo "<div class='row'>";
						echo "<div class='col-md-6'><input type='text' name='firstname' class='form-control' placeholder='Vorname' required></div>";
						echo "<div class='col-md-6'><input type='text' name='lastname' class='form-control' placeholder='Nachname' required></div>";
					echo "</div>";
				echo "</div>";
				echo "<div class='form-group'>";
					echo "<div class='row'>";
						echo "<div class='col-md-6'><input type='email' name='email1' class='form-control' placeholder='E-Mail Adresse' required></div>";
						echo "<div class='col-md-6'><input type='email' name='email2' class='form-control' placeholder='E-Mail Adresse wiederholen' required></div>";
					echo "</div>";
				echo "</div>";
				echo "<div class='form-group'>";
					echo "<div class='row'>";
						echo "<div class='col-md-6'><input type='password' name='pass1' class='form-control' placeholder='Passwort' required></div>";
						echo "<div class='col-md-6'><input type='password' name='pass2' class='form-control' placeholder='Passwort wiederholen' required></div>";
					echo "</div>";
				echo "</div>";
				echo "<div class='form-group'>";
					echo "<div class='row'>";
						echo "<div class='col-md-6'><input type='text' name='street' class='form-control' placeholder='Straße' required></div>";
						echo "<div class='col-md-6'><input type='text' name='house_number' class='form-control' placeholder='Hausnummer' required></div>";
					echo "</div>";
				echo "</div>";
				echo "<div class='form-group'>";
					echo "<div class='row'>";
						echo "<div class='col-md-6'><input type='text' name='zip' class='form-control' placeholder='Postleitzahl (PLZ)' required></div>";
						echo "<div class='col-md-6'><input type='text' name='place' class='form-control' placeholder='Ort' required></div>";
					echo "</div>";
				echo "</div>";
				echo "<div class='form-group'>";
					echo "<div class='row'>";
						echo "<div class='col-sm-4'>";
							echo "<select name='birth_d' class='form-control' required>";
								echo "<option selected disabled>Tag</option>";
								$date_d = 1;
								while ($date_d <= 31) {
									echo "<option value='".$date_d."'>".$date_d."</option>";
									$date_d++;
								}
							echo "</select>";
						echo "</div>";
						echo "<div class='col-sm-4'>";
							echo "<select name='birth_m' class='form-control' required>";
								echo "<option selected disabled>Monat</option>";
								$date_m = array("Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember");
								reset($date_m);
								while (list(, $value) = each($date_m)) {
									echo "<option value='".$value."'>".$value."</option>";
								}
							echo "</select>";
						echo "</div>";
						echo "<div class='col-sm-4'>";
							echo "<select name='birth_y' class='form-control' required>";
								echo "<option selected disabled>Jahr</option>";
								$date_Y = (date("Y") - 80);
								while ($date_Y <= (date("Y") - 10)) {
									echo "<option value='".$date_Y."'>".$date_Y."</option>";
									$date_Y++;
								}
							echo "</select>";
						echo "</div>";
					echo "</div>";
				echo "</div>";
				echo "<div class='form-group'>";
					echo "<div class='row'>";
						echo "<div class='col-md-6'><input type='text' name='country' class='form-control' placeholder='Land' required></div>";
					echo "</div>";
				echo "</div>";
				echo "<div class='form-group'>";
					echo "<div class='row'>";
						echo "<div class='col-md-6'><input type='text' name='phone_number' class='form-control' placeholder='Tel. Nr.' required></div>";
						echo "<div class='col-md-6'><input type='text' name='mobile_number' class='form-control' placeholder='Handy Nr.'></div>";
					echo "</div>";
				echo "</div>";
				echo "<button type='submit' name='register_sub' class='btn btn-success'>Registrieren und Weiter</button>";
			echo "</form>";
		}
	?>
</div>